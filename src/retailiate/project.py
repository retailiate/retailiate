# -*- coding: utf-8 -*-
from freckworks.project import FreckworksProject

from retailiate.defaults import (
    RETAILIATE_PROJECT_DIR,
    RETAILIATE_FRECKLETS_PATH,
    RETAILIATE_STATE_DIR,
    RETAILIATE_PROTOTYPE_PATH,
)


class RetailiateProject(FreckworksProject):
    def __init__(self, resolver="auto"):

        self._project_base = RETAILIATE_PROJECT_DIR
        self._context_config = ["dev"]
        self._extra_repos = [RETAILIATE_FRECKLETS_PATH]
        self._state_manager_config = {"type": "folder", "path": RETAILIATE_STATE_DIR}
        self._resolver_config = resolver
        self._prototype_repos = [RETAILIATE_PROTOTYPE_PATH]

        super(RetailiateProject, self).__init__(
            project_base_path=self._project_base,
            project_name="retailidate",
            freckles_context_config=self._context_config,
            freckles_extra_repos=self._extra_repos,
            state_manager=self._state_manager_config,
            resolver=self._resolver_config,
            prototype_repos=self._prototype_repos,
        )
