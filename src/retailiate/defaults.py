# -*- coding: utf-8 -*-
import os

from appdirs import AppDirs

retailiate_app_dirs = AppDirs("retailiate", "frkl")

RETAILIATE_PROJECT_DIR = retailiate_app_dirs.user_data_dir
RETAILIATE_STATE_DIR = os.path.join(RETAILIATE_PROJECT_DIR, "state")

RETAILIATE_RESOURCES_PATH = os.path.join(os.path.dirname(__file__), "resources")
RETAILIATE_PROTOTYPE_PATH = os.path.join(RETAILIATE_RESOURCES_PATH, "prototypes")
RETAILIATE_FRECKLETS_PATH = os.path.join(RETAILIATE_RESOURCES_PATH, "frecklets")


RETAILIATE_DICTLET_CONFIGS = [
    {
        "id": "input_vars",
        "prototype": "constant",
        "target": "machines.roles",
        "input": {"value": {"master": {"name": "retailiate"}, "db": {"name": "db"}}},
    },
    {
        "id": "provision_containers",
        "prototype": "retailiate-base-image",
        "type": "frecklet",
        "multiplier": {
            "alias": "containers",
            "type": "each_list_item",
            "repl_map": {"name": "{{ machines.roles.*.name }}"},
        },
        "input": {
            "run_config": "lxd::{{ containers.name }}",
            "vars": {"admin_user": "freckworks"},
        },
        "postprocess": {
            "type": "template",
            "template_obj": {
                "{{ __state__.containers.name }}": {
                    "admin_user": "{{ __input__.vars.admin_user }}"
                }
            },
        },
    },
    {
        "id": "create_lxd_containers",
        "prototype": "lxd-container-running",
        "type": "frecklet",
        "target": "machines.servers",
        "multiplier": {
            "alias": "containers",
            "type": "each_list_item",
            "repl_map": {"name": "{{ machines.roles.*.name }}"},
        },
        "input": {
            "run_config": "localhost",
            "vars": {
                "image_name": "debian/10",
                "name": "{{ containers.name }}",
                "image_server": "https://images.linuxcontainers.org",
                "register_target": "details",
            },
        },
        "postprocess": {
            "type": "template",
            "template_obj": {
                "{{ __state__.containers.name }}": "{{ __result__.details.addresses.eth0[0] }}"
            },
        },
    },
    # {
    #     "prototype": "register-var",
    #     "type": "frecklet",
    #     "target": "registers.ansible_var",
    #     "input": {
    #         "run_config": "localhost",
    #         "vars": {
    #             "register_target": "markus",
    #             "var_name": "ansible_python_interpreter"
    #         }
    #     }
    # },
    # {
    #     "prototype": "register-var",
    #     "type": "frecklet",
    #     "target": "machines.running",
    #     "multiplier": {
    #         "alias": "servers",
    #         "type": "each_list_item",
    #         "repl_map": {"name": "{{ machines.roles.*.name }}"},
    #     },
    #     "input": {
    #         "run_config": "lxd::{{  servers.name }}",
    #         "vars": {
    #             "register_target": "what",
    #             "var_name": "ansible_env.HOME"
    #         }
    #     },
    #     "postprocess": {
    #         "type": "template",
    #         "template_obj": {
    #             "{{ __state__.servers.name }}": "{{ __result__ }}"
    #         }
    #     }
    # }
]
