# -*- coding: utf-8 -*-
import click
from dictlets.cli.wrapper import DictletCli
from frutils.frutils_cli import logzero_option

from retailiate.defaults import RETAILIATE_DICTLET_CONFIGS
from retailiate.project import RetailiateProject


@click.group()
@logzero_option()
@click.pass_context
def cli(ctx):
    pass


@cli.group(name="debug")
@click.pass_context
def debug(ctx):

    pass


command_map = {"__default__": debug}

DictletCli.add_dictlets_commands(
    project_class=RetailiateProject,
    command_map=command_map,
    auto_initialize=RETAILIATE_DICTLET_CONFIGS,
    project_config={"resolver": "prefect"},
)
